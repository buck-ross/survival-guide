# Run with `podman run -it --rm -v ~:/root -v /tmp/.X11-unix:/tmp/.X11-unix:ro -e DISPLAY --net=host ws-base:fedora-latest`

FROM fedora:34

# Define an OCI compatible label set:
LABEL maintainer="Buckley Ross <buckleyross42@gmail.com>"

# Enumerate the commands necessary for building the image:
RUN sed -i.bak -e 's/tsflags/#tsflags/' /etc/dnf/dnf.conf && \
	dnf update -y && \
	dnf install -y make pandoc texlive texlive-cleveref texlive-datetime texlive-glossaries texlive-luaxml texlive-make4ht texlive-siunitx texlive-tex4ebook texlive-tex4ht tidy zip && \
	#dot2tex texlive-algorithmicx texlive-dot2texi texlive-frankenstein texlive-relsize texlive-tocbibind
	cd /tmp && \
	curl -Lo pandoc-crossref.tar.xz 'https://github.com/lierdakil/pandoc-crossref/releases/download/v0.3.6.4/pandoc-crossref-Linux-2.9.2.1.tar.xz' && \
	tar xJvf ./pandoc-crossref.tar.xz && \
	mv ./pandoc-crossref /usr/local/bin/ && \
	rm -fv ./pandoc*

# Setup the environment:
CMD [ "make", "-C", "/ws", "all" ]
VOLUME /ws
WORKDIR /ws

