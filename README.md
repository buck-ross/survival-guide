# A Survival Guide to Software Engineering

A collection of free guidebooks with the goal of teaching the reader the basics of survival in the software engineering profession.

## Contents Overview

The following guides are contained (or at least planned) within this collection:

- **The Buzz**: An exercise in software engineering, taught through designing & building a simple full-stack messaging app.

## Compiling the Book

If you have a good understanding of software engineering, compiling this book should be easy:

1. Install Docker (see [the docs](https://docs.docker.com/get-docker), or the appropriate section of the book itself).
2. Install Make (see [the docs](https://www.gnu.org/software/make)).
3. Build the docker image: `make docker-image`
4. Compile the book using docker: `make`

You should now have the whole book compiled, including all available distribution formats.

## License

**Copyright &copy; 2021 Buckley Ross**

This collective work is licensed under the [GNU Free Documentation License v1.3](https://www.gnu.org/licenses/fdl-1.3.html),
which is included in the compiled book under the section labeled "*GNU Free Documentation License*".

