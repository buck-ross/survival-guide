local domfilter = require 'make4ht-domfilter'

-- Define a DOM-filter to be executed across each of the generated HTML documents:
local process = domfilter {function(dom)

	-- Find & remove empty 'likeChapterHead's
	local likeChapterHeads = dom:query_selector('h2.likechapterHead')
	for _, node in ipairs(likeChapterHeads) do
		local text = node:get_text():gsub('%s+', '')
		if text == '' then
			node:remove_node()
		end
	end

	return dom
end}

Make:match("html$", process)
