DOCKER?=docker
TEXFILES = SurvivalGuide.tex the-buzz/book.tex appendix/glossary.tex appendix/sources.bib \
	appendix/fdl-1.3.tex
THE_BUZZ_MD = the-buzz/00-preface.md the-buzz/01-onboarding.md \
	the-buzz/02-version-control.md the-buzz/03-planning-a-project.md \
	the-buzz/04-the-web-ui.md the-buzz/05-the-backend-api.md \
	the-buzz/06-the-database-library.md the-buzz/07-finishing-the-sprint.md

# Define all phony targets:
.PHONY: all docker-image docker-builder clean

docker-build:
	$(DOCKER) run -it --rm -v $$(pwd):/ws survival-guide-builder

docker-image:
	$(DOCKER) build -t survival-guide-builder .

all: SurvivalGuide.pdf dist/web-mono/SurvivalGuide.html dist/web-multi/SurvivalGuide.html \
	dist/epub/SurvivalGuide.epub

clean:
	for glob in $$(cat .gitignore); do eval "if test -f $$glob || test -d $$glob; then rm -rv $$glob; else echo '>> No files matching pattern $$glob'; fi"; done
	if test -d dist; then rm -rv dist; fi


# Define targets for all of the intermediary formats:
the-buzz/book.tex: ${THE_BUZZ_MD}
	pandoc -F pandoc-crossref -M cref --top-level-division chapter -o the-buzz/book.tex ${THE_BUZZ_MD}


# Define targets for all of the deliverables:
SurvivalGuide.pdf: clean ${TEXFILES}
	# Compile the bibliography & glossary:
	pdflatex ./SurvivalGuide.tex
	biber ./SurvivalGuide
	makeglossaries ./SurvivalGuide.glo

	# The document must be compiled twice, in order to update the TOC:
	pdflatex ./SurvivalGuide.tex
	#pdflatex ./SurvivalGuide.tex

dist/web-mono/SurvivalGuide.html: SurvivalGuide.pdf domfilter.lua
	make4ht -e domfilter.lua -d ./dist/web-mono -u ./SurvivalGuide.tex 'xhtml,mathml,fn-in'

dist/web-multi/SurvivalGuide.html: SurvivalGuide.pdf domfilter.lua
	make4ht -e domfilter.lua -d ./dist/web-multi -u ./SurvivalGuide.tex 'xhtml,mathml,fn-in,2'

dist/epub/SurvivalGuide.epub: SurvivalGuide.pdf
	tex4ebook -e domfilter.lua -c epub.cfg -d ./dist/epub -f epub3 ./SurvivalGuide.tex 'xhtml,mathml,fn-in'
